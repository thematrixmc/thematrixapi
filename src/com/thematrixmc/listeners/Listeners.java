package com.thematrixmc.listeners;

import com.thematrixmc.commands.ToggleChat;
import static com.thematrixmc.matrixantispam.SwearFilter.toggled;
import static com.thematrixmc.stafftools.VGUIHandler.vanished;
import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.NameColours;
import com.thematrixmc.thematrixapi.Ranks;
import com.thematrixmc.thematrixapi.TheMatrixAPI;
import static com.thematrixmc.thematrixapi.TheMatrixAPI.TDM;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class Listeners implements Listener {

    @EventHandler
    public void onHit(PlayerInteractEvent event) {
        if (vanished.contains(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if (TDM == false) {
            NameColours.Join(event.getPlayer());
        }

        if (!toggled.contains(event.getPlayer().getName()) && Ranks.isSrStaff(event.getPlayer())) {
            toggled.add(event.getPlayer().getName());
        }

        if (!TheMatrixAPI.locations.containsKey(event.getPlayer().getName())) {
            TheMatrixAPI.locations.put(event.getPlayer().getName(), event.getPlayer().getLocation());
        }

        if (!TheMatrixAPI.afkTimer.containsKey(event.getPlayer().getName())) {
            TheMatrixAPI.afkTimer.put(event.getPlayer().getName(), 0);
        }
    }

    @EventHandler
    public void onDecay(LeavesDecayEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onForm(BlockFormEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onSpread(BlockSpreadEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        if (ToggleChat.toggle == true) {
            if (!Ranks.isStaff(player)) {
                player.sendMessage(Messages.TAG + " §6Chat disabled. Message wasn't sent.");
                event.setCancelled(true);
            }
        }
        event.setFormat("%s §7➾ §r%s");
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        event.setCancelled(event.toWeatherState());
    }

    @EventHandler
    public void preventMe(PlayerCommandPreprocessEvent event) {
        if ((event.getMessage().toLowerCase().startsWith("/me")) || event.getMessage().toLowerCase().startsWith("/op") || event.getMessage().toLowerCase().startsWith("/bukkit") || event.getMessage().toLowerCase().startsWith("/pl") || event.getMessage().toLowerCase().startsWith("/plugins") || event.getMessage().toLowerCase().startsWith("/tell") || event.getMessage().toLowerCase().startsWith("/?") || event.getMessage().toLowerCase().startsWith("/whisper") || event.getMessage().toLowerCase().startsWith("/kill")) {
            if (Ranks.isOwner(event.getPlayer())) {
                event.setCancelled(false);
            } else {
                event.getPlayer().sendMessage(Messages.noPerms);
                event.setCancelled(true);
            }

        }

        if ((event.getMessage().toLowerCase().equals("/cc")) && Ranks.isSrStaff(event.getPlayer())) {
            event.setCancelled(true);
            event.getPlayer().chat("/clearchat");
        }

        if ((event.getMessage().equalsIgnoreCase("/gm") && Ranks.isSrStaff(event.getPlayer()))) {
            event.setCancelled(true);
            event.getPlayer().sendMessage(Messages.TAG + "§cIncorrect syntax. Please use: /gmc, /gms or /gma.");
        }

        if ((event.getMessage().equalsIgnoreCase("/gms") && Ranks.isSrStaff(event.getPlayer()))) {
            event.setCancelled(true);
            event.getPlayer().chat("/gamemode s");
        }

        if ((event.getMessage().equalsIgnoreCase("/gmc") && Ranks.isSrStaff(event.getPlayer()))) {
            event.setCancelled(true);
            event.getPlayer().chat("/gamemode c");
        }

        if ((event.getMessage().equalsIgnoreCase("/gma") && Ranks.isSrStaff(event.getPlayer()))) {
            event.setCancelled(true);
            event.getPlayer().chat("/gamemode a");
        }
    }

    @EventHandler
    public void onJoin(PlayerLoginEvent event) {
        if (TheMatrixAPI.maintenanceMode == true) {
            Player player = (Player) event.getPlayer();
            if (!Ranks.isStaff(player)) {
                event.setKickMessage("§eMatrixMC §ais currently in maintenance mode. \n§6Please check back later.");
                event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
            }
        } else {
            event.setResult(PlayerLoginEvent.Result.ALLOWED);
        }
    }
}
