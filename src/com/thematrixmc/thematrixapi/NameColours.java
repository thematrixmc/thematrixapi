package com.thematrixmc.thematrixapi;

import static com.thematrixmc.commands.ToggleRank.toggled;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

public class NameColours {

    private static final ScoreboardManager manager = Bukkit.getServer().getScoreboardManager();
    private static final Scoreboard board = manager.getMainScoreboard();

    public static void Join(Player player) {
        if (toggled.contains(player)) {
            board.getTeam("Default").addPlayer(player);
        } else if (player.getName().equalsIgnoreCase("TysonPhilip") || player.getName().equalsIgnoreCase("Stargate_")) {
            board.getTeam("Friend").addPlayer(player);
        } else if (player.getName().equalsIgnoreCase("Peter_Forties")) {
            board.getTeam("Friend").addPlayer(player);
        } else if (Ranks.isOwner(player)) {
            board.getTeam("Owner").addPlayer(player);
        } else if (Ranks.isDeveloper(player)) {
            board.getTeam("Developer").addPlayer(player);
        } else if (Ranks.isAdmin(player)) {
            board.getTeam("Admin").addPlayer(player);
        } else if (Ranks.isSrMod(player)) {
            board.getTeam("SrMod").addPlayer(player);
        } else if (Ranks.isMod(player)) {
            board.getTeam("Mod").addPlayer(player);
        } else if (Ranks.isStaffManager(player)) {
            board.getTeam("SM").addPlayer(player);
        } else if (Ranks.isSrBuilder(player)) {
            board.getTeam("Builder").addPlayer(player);
        } else if (Ranks.isBuilder(player)) {
            board.getTeam("Builder").addPlayer(player);
        } else if (Ranks.isGold(player)) {
            board.getTeam("Gold").addPlayer(player);
        } else if (Ranks.isIron(player)) {
            board.getTeam("Iron").addPlayer(player);
        } else if (Ranks.isDiamond(player)) {
            board.getTeam("Diamond").addPlayer(player);
        } else if (Ranks.isYT(player)) {
            board.getTeam("YT").addPlayer(player);
        } else if (Ranks.isFriend(player)) {
            board.getTeam("Friend").addPlayer(player);
        } else {
            board.getTeam("Default").addPlayer(player);
        }
    }

    public static void Teams() {
        board.registerNewTeam("Owner").setPrefix("§a");
        board.registerNewTeam("Developer").setPrefix("§e");
        board.registerNewTeam("Admin").setPrefix("§5");
        board.registerNewTeam("SrMod").setPrefix("§4");
        board.registerNewTeam("Mod").setPrefix("§c");
        board.registerNewTeam("SM").setPrefix("§2");
        board.registerNewTeam("Builder").setPrefix("§1");
        board.registerNewTeam("Gold").setPrefix("§6");
        board.registerNewTeam("Iron").setPrefix("§7");
        board.registerNewTeam("Diamond").setPrefix("§b");
        board.registerNewTeam("Friend").setPrefix("§3");
        board.registerNewTeam("YT").setPrefix("§d");
        board.registerNewTeam("Default").setPrefix("§9");
    }

    public static void removeTeams() {
        board.getTeam("Owner").unregister();
        board.getTeam("Developer").unregister();
        board.getTeam("Admin").unregister();
        board.getTeam("SrMod").unregister();
        board.getTeam("Mod").unregister();
        board.getTeam("SM").unregister();
        board.getTeam("Builder").unregister();
        board.getTeam("Gold").unregister();
        board.getTeam("Iron").unregister();
        board.getTeam("Diamond").unregister();
        board.getTeam("Friend").unregister();
        board.getTeam("YT").unregister();
        board.getTeam("Default").unregister();

        Team all = (Team) board.getTeams();
        all.unregister();
    }
}
