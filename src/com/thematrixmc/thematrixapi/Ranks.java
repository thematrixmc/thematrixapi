package com.thematrixmc.thematrixapi;

import static com.thematrixmc.commands.ToggleRank.toggled;
import static com.thematrixmc.thematrixapi.TheMatrixAPI.TDM;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class Ranks implements Listener {

    public static boolean isOwner(Player player) {
        return PermissionsEx.getUser(player).inGroup("owner");
    }

    public static boolean isDeveloper(Player player) {
        return PermissionsEx.getUser(player).inGroup("developer");
    }

    public static boolean isStaffManager(Player player) {
        return PermissionsEx.getUser(player).inGroup("staffmanager");
    }

    public static boolean isAdmin(Player player) {
        return PermissionsEx.getUser(player).inGroup("admin");
    }

    public static boolean isSrMod(Player player) {
        return PermissionsEx.getUser(player).inGroup("seniormod");
    }

    public static boolean isMod(Player player) {
        return PermissionsEx.getUser(player).inGroup("moderator");
    }

    public static boolean isSrBuilder(Player player) {
        return PermissionsEx.getUser(player).inGroup("srbuilder");
    }

    public static boolean isBuilder(Player player) {
        return PermissionsEx.getUser(player).inGroup("builder");
    }

    public static boolean isYT(Player player) {
        return PermissionsEx.getUser(player).inGroup("youtuber");
    }

    public static boolean isFriend(Player player) {
        return PermissionsEx.getUser(player).inGroup("friend");
    }

    public static boolean isDiamond(Player player) {
        return PermissionsEx.getUser(player).inGroup("diamond");
    }

    public static boolean isGold(Player player) {
        return PermissionsEx.getUser(player).inGroup("gold");
    }

    public static boolean isIron(Player player) {
        return PermissionsEx.getUser(player).inGroup("iron");
    }

    public static boolean isDefault(Player player) {
        return PermissionsEx.getUser(player).inGroup("default");
    }

    public static boolean isPremium(Player player) {
        PermissionGroup group = PermissionsEx.getUser(player).getGroups()[0];
        return group.has("matrix.premium");
    }

    public static boolean isStaff(Player player) {
        PermissionGroup group = PermissionsEx.getUser(player).getGroups()[0];
        return group.has("matrix.staff");
    }

    public static boolean isSrStaff(Player player) {
        PermissionGroup group = PermissionsEx.getUser(player).getGroups()[0];
        return group.has("matrix.srstaff");
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        updateRank(event.getPlayer());
    }

    public static void updateRank(Player player) {
        if (toggled.contains(player)) {
            player.setDisplayName(ChatColor.BLUE + player.getName());
        } else if (player.getName().equalsIgnoreCase("TysonPhilip") || player.getName().equalsIgnoreCase("Stargate_")) {
            player.setDisplayName(ChatColor.DARK_AQUA + player.getName());
        } else if (player.getName().equalsIgnoreCase("Peter_Forties")) {
            player.setDisplayName(ChatColor.DARK_AQUA + player.getName());
        } else if (isOwner(player)) {
            player.setDisplayName(ChatColor.GREEN + player.getName());
        } else if (isDeveloper(player)) {
            player.setDisplayName(ChatColor.YELLOW + player.getName());
        } else if (isStaffManager(player)) {
            player.setDisplayName(ChatColor.DARK_GREEN + player.getName());
        } else if (isAdmin(player)) {
            player.setDisplayName(ChatColor.DARK_PURPLE + player.getName());
        } else if (isSrMod(player)) {
            player.setDisplayName(ChatColor.DARK_RED + player.getName());
        } else if (isMod(player)) {
            player.setDisplayName(ChatColor.RED + player.getName());
        } else if (isSrBuilder(player)) {
            player.setDisplayName(ChatColor.DARK_BLUE + player.getName());
        } else if (isBuilder(player)) {
            player.setDisplayName(ChatColor.DARK_BLUE + player.getName());
        } else if (isYT(player)) {
            player.setDisplayName(ChatColor.LIGHT_PURPLE + player.getName());
        } else if (isFriend(player)) {
            player.setDisplayName(ChatColor.DARK_AQUA + player.getName());
        } else if (isDiamond(player)) {
            player.setDisplayName(ChatColor.AQUA + player.getName());
        } else if (isGold(player)) {
            player.setDisplayName(ChatColor.GOLD + player.getName());
        } else if (isIron(player)) {
            player.setDisplayName(ChatColor.GRAY + player.getName());
        } else {
            player.setDisplayName(ChatColor.BLUE + player.getName());
        }
        String name = player.getDisplayName();

        if (name.length() > 16) {
            name = name.substring(0, 16);
        }
        if (TDM == false) {
            NameColours.Join(player);
        }

        player.setPlayerListName(name);
    }

    public static int getRankID(Player player) {
        if (isOwner(player)) {
            return 7;
        } else if (isDeveloper(player)) {
            return 7;
        } else if (isStaffManager(player)) {
            return 6;
        } else if (isAdmin(player)) {
            return 6;
        } else if (isSrMod(player)) {
            return 5;
        } else if (isMod(player)) {
            return 4;
        } else if (isSrBuilder(player)) {
            return 3;
        } else if (isBuilder(player)) {
            return 3;
        } else if (isYT(player)) {
            return 2;
        } else if (isFriend(player)) {
            return 2;
        } else if (isDiamond(player)) {
            return 2;
        } else if (isGold(player)) {
            return 2;
        } else if (isIron(player)) {
            return 2;
        } else {
            return 1;
        }
    }
}
