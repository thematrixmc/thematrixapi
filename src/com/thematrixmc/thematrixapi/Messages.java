package com.thematrixmc.thematrixapi;

public class Messages {

    public static String TAG = "§8\u2758 §eMatrixMC §8\u2759 §f";

    public static String STAFF = "§4§lSTAFF§f §8\u275a ";

    public static String noPerms = "Unknown command. Type \"/help\" for help.";
    
    public static String reportingTag = "§8\u2758 §eMatrix§3Reports §8\u2759 §f";
}
