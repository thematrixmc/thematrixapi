package com.thematrixmc.thematrixapi;

import com.thematrixmc.commands.Clear;
import com.thematrixmc.commands.ClearChat;
import com.thematrixmc.commands.Fly;
import com.thematrixmc.commands.GetRank;
import com.thematrixmc.commands.Help;
import com.thematrixmc.commands.List;
import com.thematrixmc.commands.Live;
import com.thematrixmc.commands.Msg;
import com.thematrixmc.commands.Rank;
import com.thematrixmc.commands.Report;
import com.thematrixmc.commands.Resign;
import com.thematrixmc.commands.Say;
import com.thematrixmc.commands.SetRank;
import com.thematrixmc.commands.Spectate;
import com.thematrixmc.commands.Staff;
import com.thematrixmc.commands.Teleport;
import com.thematrixmc.commands.ToggleChat;
import com.thematrixmc.commands.ToggleRank;
import com.thematrixmc.commands.ToggleSwears;
import com.thematrixmc.commands.UpdateRank;
import com.thematrixmc.listeners.Listeners;
import com.thematrixmc.matrixantispam.Cooldown;
import com.thematrixmc.matrixantispam.SpamProtect;
import com.thematrixmc.matrixantispam.SwearFilter;
import com.thematrixmc.stafftools.QLHandler;
import com.thematrixmc.stafftools.STCommand;
import com.thematrixmc.stafftools.STGUIHandler;
import com.thematrixmc.stafftools.SpectateGUI;
import com.thematrixmc.stafftools.VGUIHandler;
import static com.thematrixmc.thematrixapi.TheMatrixAPI.maintenanceMode;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class TheMatrixAPI extends JavaPlugin {

    public static Connection conn;

    public static Map<String, Location> locations = new HashMap<>();
    public static Map<String, Integer> afkTimer = new HashMap<>();

    String url = getConfig().getString("url");
    String user = getConfig().getString("user");
    String pass = getConfig().getString("password");

    private int rankUpdateTime = getConfig().getInt("rankUpdateTime");

    private static Plugin plugin;
    public static boolean maintenanceMode;
    public static boolean TDM;
    public static boolean FFA;
    public static boolean Hub;

    public static Class main = TheMatrixAPI.class;

    public long rankUpdateDelay = 20 * 60 * rankUpdateTime;

    @Override
    public void onEnable() {

        FFA = false;
        TDM = false;
        Hub = false;
        maintenanceMode = false;

        getCommand("togglerank").setExecutor(new ToggleRank());
        getCommand("getrank").setExecutor(new GetRank());
        getCommand("list").setExecutor(new List());
        getCommand("os").setExecutor(new Staff());
        getCommand("so").setExecutor(new Staff());
        getCommand("onlinestaff").setExecutor(new Staff());
        getCommand("staffonline").setExecutor(new Staff());
        getCommand("staff").setExecutor(new Staff());
        getCommand("ranks").setExecutor(new Rank());
        getCommand("resign").setExecutor(new Resign());
        getCommand("clear").setExecutor(new Clear());
        getCommand("vanish").setExecutor(new VGUIHandler());
        getCommand("spectate").setExecutor(new Spectate());
        getCommand("live").setExecutor(new Live());
        getCommand("clearchat").setExecutor(new ClearChat());
        getCommand("togglechat").setExecutor(new ToggleChat());
        getCommand("setrank").setExecutor(new SetRank());
        getCommand("updaterank").setExecutor(new UpdateRank());
        getCommand("msg").setExecutor(new Msg());
        getCommand("toggleswears").setExecutor(new ToggleSwears());
        getCommand("fly").setExecutor(new Fly());
        getCommand("Help").setExecutor(new Help());
        getCommand("report").setExecutor(new Report());
        getCommand("say").setExecutor(new Say());
        getCommand("tp").setExecutor(new Teleport());
        getCommand("ST").setExecutor(new STCommand());

        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new Ranks(), this);
        pm.registerEvents(new Listeners(), this);
        pm.registerEvents(new QLHandler(), this);
        pm.registerEvents(new STGUIHandler(), this);
        pm.registerEvents(new SpectateGUI(), this);
        pm.registerEvents(new VGUIHandler(), this);
        pm.registerEvents(new SwearFilter(), this);
        pm.registerEvents(new SpamProtect(), this);
        pm.registerEvents(new Cooldown(), this);

        getLogger().info("API has been enabled!");

        if (!new File(this.getDataFolder(), "config.yml").exists()) {
            this.saveDefaultConfig();
        }

        try {
            conn = DriverManager.getConnection(url, user, pass);
        } catch (SQLException ex) {
            getLogger().info("Database and connection enabled - Lemons not loaded.");
        }

        try {
            Class.forName("me.joegameplay.uhclobby.Lobby");
            TDM = true;
            getLogger().info("TDM found! API adapted.");
        } catch (ClassNotFoundException e) {
            TDM = false;
            getLogger().info("TDM not found!");
        }
        try {
            Class.forName("com.thematrixmc.ffa.Core");
            FFA = true;
            getLogger().info("FFA found! API adapted.");
        } catch (ClassNotFoundException e) {
            FFA = false;
            getLogger().info("FFA not found!");
        }
        try {
            Class.forName("com.thematrixmc.matrixnetworkhub.Core");
            Hub = true;
            getLogger().info("Hub found! API adapted.");
        } catch (ClassNotFoundException e) {
            Hub = false;
            getLogger().info("Hub not found!");
        }
        if (TDM == false) {
            NameColours.Teams();
        }

        rankUpdateTime = Bukkit.getScheduler().scheduleSyncRepeatingTask(getPlugin(main), new Runnable() {
            @Override
            public void run() {
                if (Bukkit.getOnlinePlayers().length > 0) {
                    for (Player all : Bukkit.getOnlinePlayers()) {
                        Ranks.updateRank(all);
                        if (Ranks.isSrStaff(all)) {
                            all.sendMessage(Messages.STAFF + "§6Updated all ranks successfully.");
                            System.out.println("Updated ranks at server ffa.");
                        }
                    }
                } else {
                    System.out.println("Couldn't update ranks - there aren't any players here!");
                    System.out.println("Will try again in 15 minutes...");
                }
            }
        }, 0L, rankUpdateDelay);

        if (!Hub) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(getPlugin(main), new Runnable() {
                @Override
                public void run() {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        if (locations.containsKey(player.getName())) {
                            if (locations.get(player.getName()).equals(player.getLocation())) {
                                if (afkTimer.containsKey(player.getName())) {
                                    int current = afkTimer.get(player.getName());
                                    if (current != 20) {
                                        afkTimer.remove(player.getName());
                                        afkTimer.put(player.getName(), current + 1);
                                        System.out.println("Added 1 minute to " + player.getName() + "'s afk timer - they're now at " + (20 - current) + " minutes before kickToHub.");
                                    } else {
                                        player.sendMessage(Messages.TAG + "§cDue to being AFK for more than 20 minutes, you were returned to the hub.");
                                        bungeekick(player);
                                        System.out.println("Kicked " + player.getName() + " backToHub.");
                                        afkTimer.remove(player.getName());
                                        locations.remove(player.getName());
                                    }
                                } else {
                                    afkTimer.put(player.getName(), 1);
                                    System.out.println("Added 1 minute to " + player.getName() + "'s afk timer - they're now at 19 minutes before kickToHub.");
                                }
                            } else {
                                locations.remove(player.getName());
                                locations.put(player.getName(), player.getLocation());
                                System.out.println("Reset " + player.getName() + "'s last afk location.");
                            }
                        } else {
                            locations.put(player.getName(), player.getLocation());
                            System.out.println("Set " + player.getName() + "'s last afk location.");
                        }

                    }
                }
            }, 0L, 60 * 20L);
        }
    }

    @Override
    public void onDisable() {
        if (TDM == false) {
            NameColours.removeTeams();
        }

        Bukkit.getServer().getScheduler().cancelTask(maintenance);
        getLogger().info("API has been disabled!");

        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
        } catch (SQLException e) {
        }
    }

    public static Plugin getPlugin() {
        return plugin;
    }

    public static void bungeekick(Player pkick) {
        Bukkit.getMessenger().registerOutgoingPluginChannel(getPlugin(main), "BungeeCord");

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("ConnectOther");
            out.writeUTF(pkick.getName());
            out.writeUTF("hub");
        } catch (IOException localIOException) {
        }
        pkick.sendPluginMessage(getPlugin(main), "BungeeCord", b.toByteArray());
    }

    public int maintenance = 1;
    public int timeInSeconds;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (cmd.getName().equalsIgnoreCase("countdown")) {
                sender.sendMessage(Messages.TAG + " §aTime left until maintenance " + timeInSeconds);
            } else if (Ranks.isSrStaff(player)) {
                if (cmd.getName().equalsIgnoreCase("maintenance")) {
                    maintenance(sender);
                } else if (cmd.getName().equalsIgnoreCase("cancelcountdown")) {
                    Bukkit.getServer().getScheduler().cancelTask(maintenance);
                    Bukkit.broadcastMessage(Messages.TAG + " §aMaintence countdown has been canceled.");
                    maintenanceMode = false;
                    sender.sendMessage(Messages.TAG + "§aMaintenance mode disabled.");
                } else if (cmd.getName().equalsIgnoreCase("instantmaintenance")) {
                    instantMaintenance(sender);
                    sender.sendMessage(Messages.TAG + "§aMaintenance mode enabled.");
                }
            }
        } else {
            if (cmd.getName().equalsIgnoreCase("countdown")) {
                maintenance(sender);
            } else if (cmd.getName().equalsIgnoreCase("countdown")) {
                sender.sendMessage(Messages.TAG + " §aTime left until maintenance " + timeInSeconds);
            } else if (cmd.getName().equalsIgnoreCase("cancelcountdown")) {
                Bukkit.getServer().getScheduler().cancelTask(maintenance);
                Bukkit.broadcastMessage(Messages.TAG + " §aMaintence countdown has been canceled.");
                maintenanceMode = false;
                sender.sendMessage(Messages.TAG + "§aMaintenance mode disabled.");
            } else if (cmd.getName().equalsIgnoreCase("instantmaintenance")) {
                instantMaintenance(sender);
                sender.sendMessage(Messages.TAG + "§aMaintenance mode enabled.");
            }
        }
        return false;
    }

    public void instantMaintenance(CommandSender sender) {
        maintenanceMode = true;
        for (Player pl : Bukkit.getOnlinePlayers()) {
            if (!Ranks.isStaff(pl)) {
                pl.kickPlayer("§eMatrixMC §ais currently in maintenance mode. \n§6Please check back later.");
            }
            if (Bukkit.getServer().getScheduler().isCurrentlyRunning(maintenance)) {
                Bukkit.getServer().getScheduler().cancelTask(maintenance);
            }
        }
    }

    public void maintenance(final CommandSender sender) {
        if (maintenanceMode == true) {
            maintenanceMode = false;
            sender.sendMessage(Messages.TAG + "§aMaintenance mode disabled.");
            Bukkit.getServer().getScheduler().cancelTask(maintenance);
        } else {
            timeInSeconds = 61;
            maintenance = getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

                @Override
                public void run() {
                    timeInSeconds--;
                    if (timeInSeconds == 60) {
                        Bukkit.broadcastMessage(Messages.TAG + "§aServer is going into §6Maintenance Mode §ain 1 minute.");
                    } else if (timeInSeconds == 10) {
                        Bukkit.broadcastMessage(Messages.TAG + "§aServer is going into §6Maintenance Mode §ain " + timeInSeconds + " seconds.");
                    } else if (timeInSeconds == 5) {
                        Bukkit.broadcastMessage(Messages.TAG + "§aServer is going into §6Maintenance Mode §ain " + timeInSeconds + " seconds.");
                    } else if (timeInSeconds == 4) {
                        Bukkit.broadcastMessage(Messages.TAG + "§aServer is going into §6Maintenance Mode §ain " + timeInSeconds + " seconds.");
                    } else if (timeInSeconds == 3) {
                        Bukkit.broadcastMessage(Messages.TAG + "§aServer is going into §6Maintenance Mode §ain " + timeInSeconds + " seconds.");
                    } else if (timeInSeconds == 2) {
                        Bukkit.broadcastMessage(Messages.TAG + "§aServer is going into §6Maintenance Mode §ain " + timeInSeconds + " seconds.");
                    } else if (timeInSeconds == 1) {
                        Bukkit.broadcastMessage(Messages.TAG + "§aServer is going into §6Maintenance Mode §ain " + timeInSeconds + " second.");
                    } else if (timeInSeconds == 0) {
                        instantMaintenance(sender);
                    }
                }
            }, 20l, 20l);
        }
    }
}
