package com.thematrixmc.stafftools;

import com.thematrixmc.thematrixapi.Ranks;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class STCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String args[]) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (Ranks.isStaff(player)) {
                player.openInventory(STGUI.getSTGUI());
            } else {
            }
        }
        return false;
    }

}
