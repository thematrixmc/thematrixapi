package com.thematrixmc.stafftools;

import com.thematrixmc.thematrixapi.Messages;
import static com.thematrixmc.thematrixapi.TheMatrixAPI.FFA;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class STGUIHandler implements Listener {

    private final SpectateGUI spectategui = new SpectateGUI();

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getInventory().getTitle().equalsIgnoreCase("[ST] Select a tool!")) {
            event.setCancelled(true);
            Player player = (Player) event.getWhoClicked();
            ItemStack clicked = event.getCurrentItem();
            if (event.getCurrentItem().getType() == Material.WOOL) {
                if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§bSpectate")) {
                    if (FFA == false) {
                        player.sendMessage(Messages.TAG + "§cYou don't have permission to use this in this server.");
                    } else {
                        spectategui.openSpectateGUI(player, Bukkit.getOnlinePlayers(), 1, new ItemStack(Material.SKULL_ITEM, 1, (byte) 3));
                    }
                }
                if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§eQuick Links")) {
                    player.closeInventory();
                    player.openInventory(QLGUI.getQLGUI());
                }
                if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aVanish")) {
                    if (FFA == false) {
                        player.sendMessage(Messages.TAG + "§cYou don't have permission to use this in hub.");
                    } else {
                        player.closeInventory();
                        player.openInventory(VGUI.getVGUI());
                    }
                }
            }
        }
    }
}
