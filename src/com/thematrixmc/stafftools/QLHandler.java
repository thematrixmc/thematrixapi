package com.thematrixmc.stafftools;

import com.thematrixmc.thematrixapi.Messages;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Brandons Account
 */
public class QLHandler implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getInventory().getTitle().equalsIgnoreCase("[ST] Quick Links")) {
            if (event.getCurrentItem() != null && event.getCurrentItem().getType() != null) {
                event.setCancelled(true);
                Player player = (Player) event.getWhoClicked();
                if (event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().hasDisplayName()) {
                    event.setCancelled(true);
                    ItemStack clicked = event.getCurrentItem();
                    if (clicked != null) {
                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aWebsite/Forums §4(Don't post in chat yet!)")) {
                            player.chat("At the moment the website is in development, but for now you can view the forums at http://forum.thematrixmc.com.");

                        }
                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aReport Rule Breakers")) {
                            player.chat("Report rule breakers here: http://bit.ly/1of7cVW");

                        }

                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aAppeal Bans")) {
                            player.chat("Appeal bans by joining the TeamSpeak and talking to a Senior Staff member. TS IP: matrixmc.eu");
                        }

                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aFeedback")) {
                            player.chat("Submit your feedback and or suggestions here: support.thematrixmc.com");
                        }

                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aTeamSpeak")) {
                            player.chat("Matrix Teamspeak IP: matrixmc.eu");
                        }

                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aSwearing")) {
                            player.chat("Watch the langauge please.");
                        }

                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aCaps")) {
                            player.chat("Watch the caps please.");
                        }

                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aDisrespect")) {
                            player.chat("Please do not disrespect other players.");
                        }

                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aStaff Disrespect")) {
                            player.chat("Please do not disrespect staff.");

                        }

                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aSpam")) {
                            player.chat("Please do not spam.");
                        }

                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aDirty Chat")) {
                            player.chat("Please keep chat clean.");
                        }

                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aBreaking rules [LAST WARNING]")) {
                            player.chat("Stop breaking the rules! This is your LAST warning!");
                        }

                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aStaff Guide")) {
                            player.sendMessage(Messages.TAG + "§3Matrix staff guide: §a§lhttp://bit.ly/1pJvsgZ");
                        }

                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aBan Report")) {
                            player.sendMessage(Messages.TAG + "§3Report bans here: §a§lhttp://www.bit.ly/1kVWKhl");
                        }
                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aPremium")) {
                            player.chat("Purchase premium here: store.thematrixmc.com");
                        }
                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aRanks")) {
                            player.chat("All of the MatrixMC ranks: §aOwner§f, §eDeveloper§f, §2StaffManager§f, §5Admin§f, §4SeniorMod§f, §cModerator§f, §1Builder§f, §dYoutuber§f, §3Friend§f, §bDiamond§f, §6Gold§f, §7Iron§f, and §9Default");
                        }
                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aMod App")) {
                            player.chat("Apply for Moderator here: http://bit.ly/1teQ4mz");
                        }
                        if (event.getCurrentItem().getItemMeta().getDisplayName().contains("§aMap submissions")) {
                            player.chat("Submit your maps to Matrix here: http://bit.ly/MatrixMaps");
                        }
                    }

                }
            }
        }
    }

}
