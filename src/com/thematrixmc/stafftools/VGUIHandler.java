package com.thematrixmc.stafftools;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import static com.thematrixmc.thematrixapi.TheMatrixAPI.FFA;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class VGUIHandler implements Listener, CommandExecutor {

    public static ArrayList<Player> vanished = new ArrayList<>();

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getInventory().getTitle().equalsIgnoreCase("[ST] Click to toggle vanish!")) {
            if (event.getCurrentItem() != null && event.getCurrentItem().getType() != null) {
                event.setCancelled(true);
                Player player = (Player) event.getWhoClicked();
                if (event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().hasDisplayName()) {
                    event.setCancelled(true);
                    ItemStack clicked = event.getCurrentItem();
                    if (clicked != null) {
                        if (clicked.getItemMeta().getDisplayName().contains("§aVanish \u27a0 Enable")) {
                            for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
                                if (!Ranks.isSrStaff(pl)) {
                                    pl.hidePlayer(player);
                                }
                            }
                            player.setNoDamageTicks(999999999);
                            player.setAllowFlight(true);
                            vanished.add(player);
                            player.closeInventory();
                            player.sendMessage(Messages.TAG + "§6Vanish mode toggled - " + "§aEnabled§6.");
                        } else if (clicked.getItemMeta().getDisplayName().contains("§cVanish \u27a0 Disable")) {
                            for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
                                pl.showPlayer(player);
                            }
                            player.setNoDamageTicks(0);
                            if (player.getGameMode() == GameMode.CREATIVE) {
                            } else {
                                player.setAllowFlight(false);
                            }
                            vanished.remove(player);
                            player.closeInventory();
                            player.sendMessage(Messages.TAG + "§6Vanish mode toggled - " + "§cDisabled§6.");
                        }
                    }
                }
            }

        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        if (!Ranks.isSrStaff(player)) {
            for (Player p : vanished) {
                player.hidePlayer(p);
            }
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        if (vanished.contains(player)) {
            vanished.remove(player);
            for (Player p : Bukkit.getOnlinePlayers()) {
                e.getPlayer().showPlayer(player);
            }
            player.setNoDamageTicks(0);
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (Ranks.isStaff(player)) {
                if (FFA == false) {
                    player.sendMessage(Messages.TAG + "§cYou don't have permission to use this in hub.");
                } else if (!vanished.contains(player)) {
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        if (!Ranks.isSrStaff(pl)) {
                            pl.hidePlayer(player);
                        }
                    }
                    player.setAllowFlight(true);
                    vanished.add(player);
                    player.setNoDamageTicks(999999999);
                    player.sendMessage(Messages.TAG + "§6Vanish mode toggled - " + "§aEnabled§6.");
                } else {
                    for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
                        pl.showPlayer(player);
                    }
                    if (player.getGameMode() == GameMode.CREATIVE) {
                    } else {
                        player.setAllowFlight(false);
                    }
                    player.setNoDamageTicks(0);
                    vanished.remove(player);
                    player.sendMessage(Messages.TAG + "§6Vanish mode toggled - " + "§cDisabled§6.");
                }
            }
        }
        return false;
    }
}
