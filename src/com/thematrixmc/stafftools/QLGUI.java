package com.thematrixmc.stafftools;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class QLGUI {

    public static Inventory getQLGUI() {
        Inventory inv = Bukkit.createInventory(null, 18, "[ST] Quick Links");
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aWebsite/Forums §4(Don't post in chat yet!)");
            List<String> lore = new ArrayList<>();
            lore.add("§fAt the moment the website is in development, but for");
            lore.add("§fnow you can view the forums at http://forum.thematrixmc.com.");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aReport Rule Breakers");
            List<String> lore = new ArrayList<>();
            lore.add("§fReport rule breakers here: http://bit.ly/1of7cVW");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aAppeal Bans");
            List<String> lore = new ArrayList<>();
            lore.add("§fAppeal bans by joining the TeamSpeak");
            lore.add("§fand talking to a Senior Staff member.");
            lore.add("§fTS IP: matrixmc.eu");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aFeedback");
            List<String> lore = new ArrayList<>();
            lore.add("§fSubmit your feedback and/or suggestions here: support.thematrixmc.com");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aTeamSpeak");
            List<String> lore = new ArrayList<>();
            lore.add("§fMatrix Teamspeak IP: matrixmc.eu");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aPremium");
            List<String> lore = new ArrayList<>();
            lore.add("§fPurchase premium here: store.thematrixmc.com");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aSwearing");
            List<String> lore = new ArrayList<>();
            lore.add("§fWatch the langauge please.");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aCaps");
            List<String> lore = new ArrayList<>();
            lore.add("§fWatch the caps please.");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aDisrespect");
            List<String> lore = new ArrayList<>();
            lore.add("§fPlease do not disrespect other players.");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aStaff Disrespect");
            List<String> lore = new ArrayList<>();
            lore.add("§fPlease do not disrespect staff.");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aSpam");
            List<String> lore = new ArrayList<>();
            lore.add("§fPlease do not spam.");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aDirty Chat");
            List<String> lore = new ArrayList<>();
            lore.add("§fPlease keep chat clean.");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aBreaking rules [LAST WARNING]");
            List<String> lore = new ArrayList<>();
            lore.add("§fStop breaking the rules! This is your LAST warning!");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aStaff Guide");
            List<String> lore = new ArrayList<>();
            lore.add("§fSends you the staff guide!");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aBan Report");
            List<String> lore = new ArrayList<>();
            lore.add("§fSends you the link to report ALL of your bans!");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aRanks");
            List<String> lore = new ArrayList();
            lore.add("§fList all ranks!");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(new ItemStack[]{item});
        }
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aMod App");
            List<String> lore = new ArrayList<>();
            lore.add("§fSend link for Mod App.");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        return inv;
    }
}
