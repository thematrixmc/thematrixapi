package com.thematrixmc.matrixantispam;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import java.util.HashMap;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class SpamProtect implements Listener {

    HashMap<String, String> uuidMessage = new HashMap();

    @EventHandler
    public void onAsync(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        if (!(Ranks.isStaff(player))) {
            if (this.uuidMessage.containsKey(player.getName())) {
                if (((String) this.uuidMessage.get(player.getName())).equals(event.getMessage())) {
                    event.setCancelled(true);
                    player.sendMessage(Messages.TAG + ChatColor.DARK_AQUA + "Please don't repeat yourself! If you do you face a span-ban.");
                } else {
                    uuidMessage.remove(player.getName());
                    uuidMessage.put(player.getName(), event.getMessage());
                }
            } else {
                uuidMessage.put(player.getName(), event.getMessage());
            }
        }
    }
}
