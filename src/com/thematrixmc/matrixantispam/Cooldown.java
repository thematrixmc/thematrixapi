package com.thematrixmc.matrixantispam;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Cooldown implements Listener {

    private final Map<String, Long> lastUsage = new HashMap();
    private final int cdtime = 3;

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        if (!(Ranks.isPremium(player))) {
            long lastUsed = 0L;
            if (this.lastUsage.containsKey(player.getName())) {
                lastUsed = (this.lastUsage.get(player.getName()));
            }
            int cdmillis = cdtime * 1000;
            if (System.currentTimeMillis() - lastUsed >= cdmillis) {
                this.lastUsage.put(player.getName(), System.currentTimeMillis());
            } else {
                event.setCancelled(true);
                int timeLeft = (int) (5L - (System.currentTimeMillis() - lastUsed) / 1000L);
                player.sendMessage(Messages.TAG + ChatColor.GRAY + "You must wait " + timeLeft + " seconds before sending another message!");
            }
        }
    }
}
