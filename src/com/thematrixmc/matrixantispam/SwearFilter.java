package com.thematrixmc.matrixantispam;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class SwearFilter implements Listener {

    public static ArrayList<String> toggled = new ArrayList<>();

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        String msg = event.getMessage().toLowerCase();
        if (event.getMessage().equals(event.getMessage().toUpperCase()) && (event.getMessage().length() > 2)) {
            event.setMessage(event.getMessage().toLowerCase());
        }

        File filter = new File("API files/swearfilter.txt");
        try (BufferedReader br = new BufferedReader(new FileReader(filter))) {
            for (String line; (line = br.readLine()) != null;) {
                if (msg.contains(line.toLowerCase())) {
                    event.setCancelled(true);
                    event.getPlayer().sendMessage(Messages.TAG + "§cPlease do not swear. You WILL be banned if you bypass this.");
                    br.close();
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        if (Ranks.isStaff(pl)) {
                            pl.sendMessage(Messages.STAFF + event.getPlayer().getDisplayName() + " §cattempted to swear.");
                            if(toggled.contains(pl.getName())){
                                pl.sendMessage(Messages.STAFF + "§cWhat was said: §6" + msg); 
                            }else{
                                if(Ranks.isSrStaff(pl)){
                                    pl.sendMessage(Messages.STAFF + "§cTo see what was said next time, use /toggleswears.");
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException ex) {
        }

        if ((msg.contains("tttt"))
                || (msg.contains("hhhh"))) {
            event.setCancelled(true);
            event.getPlayer().sendMessage(Messages.TAG + "§cPlease don't spam.");
        }
        if (!Ranks.isStaff(event.getPlayer())) {
            for (String str : event.getMessage().split(" ")) {
                if (str.equalsIgnoreCase("ez")) {
                    event.setCancelled(true);
                    event.getPlayer().sendMessage(Messages.TAG + "§cPlease do not disrepect other players.");
                }
            }
            if ((msg.contains("noob"))
                    || (msg.contains("eZ"))
                    || (msg.contains("learn to play"))
                    || (msg.equalsIgnoreCase("e.z"))
                    || (msg.contains("shrekt"))
                    || (msg.contains("r3kt"))
                    || (msg.contains("scrub"))
                    || (msg.contains("rekt"))
                    || (msg.contains("l2p"))
                    || (msg.contains("l2p"))) {
                event.setCancelled(true);
                event.getPlayer().sendMessage(Messages.TAG + "§cPlease do not disrepect other players.");
            }
            if ((msg.contains("hack"))
                    || (msg.contains("h4x0r"))
                    || (msg.contains("haack"))
                    || (msg.contains("hak"))
                    || (msg.contains("haax"))
                    || (msg.contains("haaack"))
                    || (msg.contains("haaak"))
                    || (msg.contains("hax"))
                    || (msg.contains("fast heal"))
                    || (msg.contains("macro"))
                    || (msg.contains("killaura"))
                    || (msg.contains("aimbot"))) {
                event.setCancelled(true);
                event.getPlayer().sendMessage(Messages.TAG + "§cPlease use /report to report a hacker.");
            }
        }
    }
}
