package com.thematrixmc.currencies;

import static com.thematrixmc.thematrixapi.TheMatrixAPI.conn;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.bukkit.OfflinePlayer;

public class BulletAPI {

    public static int getBullets(OfflinePlayer p) throws SQLException {
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM `bullets` WHERE `UUID`='" + p.getUniqueId().toString() + "';");
        if (!rs.next()) {
            return 0;
        }
        return rs.getInt("Bullets");
    }

    public static void addBullets(OfflinePlayer p, int i) throws SQLException, ClassNotFoundException {
        Statement statement = conn.createStatement();
        int bullets = getBullets(p);
        if (bullets != 0) {
            statement.executeUpdate("UPDATE `bullets` SET `Bullets`='" + (bullets + i) + "' WHERE `UUID`='" + p.getUniqueId().toString() + "';");
        } else {
            statement.executeUpdate("INSERT INTO `bullets` (`UUID`,`Bullets`,`Name`) VALUES ('" + p.getUniqueId().toString() + "','" + i + "', '" + p.getName() + "');");
        }
    }

    public static void setBullets(OfflinePlayer p, int i) throws SQLException, ClassNotFoundException {
        Statement statement = conn.createStatement();
        int bullets = getBullets(p);
        if (bullets != 0) {
            statement.executeUpdate("UPDATE `bullets` SET `Bullets`='" + (i) + "' WHERE `UUID`='" + p.getUniqueId().toString() + "';");
        } else {
            statement.executeUpdate("INSERT INTO `bullets` (`UUID`,`Bullets`,`Name`) VALUES ('" + p.getUniqueId().toString() + "','" + i + "', '" + p.getName() + "');");
        }
    }

    public static void takeBullets(OfflinePlayer p, int i) throws SQLException, ClassNotFoundException {
        Statement statement = conn.createStatement();
        int bullets = getBullets(p);
        if (bullets != 0) {
            statement.executeUpdate("UPDATE `bullets` SET `Bullets`='" + (bullets - i) + "' WHERE `UUID`='" + p.getUniqueId().toString() + "';");
        } else {
            statement.executeUpdate("INSERT INTO `bullets` (`UUID`,`Bullets`,`Name`) VALUES ('" + p.getUniqueId().toString() + "','" + i + "', '" + p.getName() + "');");
        }
    }
}
