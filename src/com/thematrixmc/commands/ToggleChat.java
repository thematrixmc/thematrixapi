package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ToggleChat implements CommandExecutor {

    public static boolean toggle;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        Player player = (Player) sender;
        if (Ranks.isStaff(player)) {
            if (toggle == false) {
                toggle = true;
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    if (Ranks.isStaff(player)) {
                        pl.sendMessage(Messages.TAG + "§6Chat was toggled off by " + player.getDisplayName() + "§6.");
                    }
                }
            } else if (toggle == true) {
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    toggle = false;
                    if (Ranks.isStaff(player)) {
                        pl.sendMessage(Messages.TAG + "§6Chat was toggled on by " + player.getDisplayName() + "§6.");
                    }
                }
            }
        }
        return false;
    }
}
