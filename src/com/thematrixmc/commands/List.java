package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class List implements CommandExecutor {

    int Staff;
    int Other;
    int Prem;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            sender.sendMessage(Messages.TAG + "§aPlayers online: " + Bukkit.getOnlinePlayers().length);
            sender.sendMessage(Messages.TAG + "§aStaff: " + Staff());
            sender.sendMessage(Messages.TAG + "§aPremiums: " + Premiums());
            sender.sendMessage(Messages.TAG + "§aOthers: "+ Other());
        }
        return false;
    }

    public int Staff() {
        Staff = 0;
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (Ranks.isStaff(player)) {
                Staff = Staff + 1;
            }
        }
        return Staff;
    }

    public int Premiums() {
        Other = 0;
        for (Player player : Bukkit.getOnlinePlayers()) {
            if ((Ranks.isPremium(player)) && (!Ranks.isStaff(player))) {
                Prem = Prem + 1;
            }
        }
        return Prem;
    }

    public int Other() {
        Prem = 0;
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!Ranks.isPremium(player)) {
                Other = Other + 1;
            }
        }
        return Other;
    }
}