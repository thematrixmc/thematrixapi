package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import static com.thematrixmc.thematrixapi.TheMatrixAPI.FFA;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Spectate implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        Player player = (Player) sender;
        if (sender instanceof Player) {
            if (Ranks.isStaff(player)) {
                if (FFA == false) {
                    player.sendMessage(Messages.TAG + "§cYou don't have permission to use this in hub.");
                } else if (args.length == 1) {
                    if (!(Bukkit.getPlayer(args[0]) == null)) {
                        if (player.getAllowFlight() == true) {
                            Location target = Bukkit.getPlayer(args[0]).getLocation();
                            player.teleport(target);
                        } else {
                            player.sendMessage(Messages.TAG + "§cThis commmand can only be used when in vanish.");
                        }
                    }
                } else {
                    sender.sendMessage(Messages.TAG + "§cInvalid syntax. Please use /spectate [player]");
                }
            }
        }
        return false;
    }
}
