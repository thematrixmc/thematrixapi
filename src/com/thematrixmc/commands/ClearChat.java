package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClearChat implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        Player player = (Player) sender;
        if ((sender instanceof Player)) {
            if (Ranks.isStaff(player)) {
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage("");
                    pl.sendMessage(Messages.TAG + " \u00A7aChat cleared by a staff member.");
                }
            }
        }
        return false;
    }
}
