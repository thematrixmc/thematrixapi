package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Staff implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if ((cmd.getName().equalsIgnoreCase("staffonline"))
                || (cmd.getName().equalsIgnoreCase("os"))
                || (cmd.getName().equalsIgnoreCase("so")) 
                || (cmd.getName().equalsIgnoreCase("onlinestaff"))) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (!(Ranks.isStaff(player))) {
                    if (!"".equals(Player())) {
                        sender.sendMessage("§8\u2758 §eMatrixMC " + Player() + "§8.");
                    } else {
                        sender.sendMessage(Messages.TAG + " §cThere are currently no staff online.");
                    }
                } else {
                }
            }
        } else if (cmd.getName().equalsIgnoreCase("staff")) {
            if (sender instanceof Player) {
                if (!"".equals(Player())) {
                    Player player = (Player) sender;
                    sender.sendMessage("§8\u2758 §eMatrixMC " + Player() + "§8."); 
                } else {
                    sender.sendMessage(Messages.TAG + " §cThere are currently no staff online.");
                }
            }
        }
        return false;
    }

    private String Player() {
        String staff = "";
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (Ranks.isStaff(player) && !(player.getName().equalsIgnoreCase("TysonPhilip")) && !(player.getName().equalsIgnoreCase("Stargate_")) && !(player.getName().equalsIgnoreCase("Peter_Forties"))) {
                staff = staff + " \u00A78\u2759 " + player.getDisplayName();
            }
        }
        return staff;
    }
}
