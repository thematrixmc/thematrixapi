package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Fly implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String args[]) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length > 2) {

            } else {
                if (args.length == 0) {
                    if (Ranks.isSrStaff(player)) {
                        if (player.getAllowFlight() == false) {
                            player.setAllowFlight(true);
                            player.sendMessage(Messages.TAG + "§6Flight has been toggled - §a§lEnabled!");
                        } else {
                            player.setAllowFlight(false);
                            player.sendMessage(Messages.TAG + "§6Flight has been toggled - §c§lDisabled!");
                        }
                    } else {
                        sender.sendMessage(Messages.noPerms);
                    }
                } else {
                    if (Ranks.isSrStaff(player)) {
                        Player target = Bukkit.getPlayerExact(args[0]);
                        if (target == null || !target.isOnline()) {

                            player.sendMessage(Messages.TAG + "§cPlayer not online.");

                        } else {
                            if (target.isDead()) {
                                player.sendMessage(Messages.TAG + "§cPlayer is currently dead - cannot activate flymode!");
                            } else {
                                if (target.getAllowFlight() == false) {
                                    target.setAllowFlight(true);
                                    player.sendMessage(Messages.TAG + "§6Flight for " + target.getDisplayName() + " §6has been toggled - §a§lEnabled!");
                                    target.sendMessage(Messages.TAG + "§aYou've been allowed to fly! Hooray!");
                                } else {
                                    target.setAllowFlight(false);
                                    player.sendMessage(Messages.TAG + "§6Flight for " + target.getDisplayName() + " §6has been toggled - §c§lDisabled!");
                                    target.sendMessage(Messages.TAG + "§cYour fly mode has been disabled. Sad alex :(");
                                }
                            }

                        }
                    }else{
                        player.sendMessage(Messages.noPerms);
                    }
                }
            }
        }
        return false;
    }

}
