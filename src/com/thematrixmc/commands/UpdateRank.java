package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UpdateRank implements CommandExecutor {

    @Override
    @SuppressWarnings("depreciation")
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (Ranks.isSrStaff(player) && !Ranks.isSrMod(player)) {
                if (args.length == 0 || args.length > 1) {
                    if(args.length == 0){
                        Ranks.updateRank(player);
                        sender.sendMessage(Messages.TAG + "§aYour rank has been updated.");
                    }else{
                        sender.sendMessage(Messages.TAG + "§cUsage: /updaterank [user/all]");
                    }
                } else {
                    if (args[0].equalsIgnoreCase("all")) {
                        if(Bukkit.getOnlinePlayers().length >= 1){
                            for(Player all : Bukkit.getOnlinePlayers()){
                                Ranks.updateRank(all);
                            }
                            player.sendMessage(Messages.TAG + "§aAll ranks updated.");
                        }else{
                            player.sendMessage(Messages.TAG + "§cNo players online.");
                        }
                    } else {
                        Player target = Bukkit.getPlayerExact(args[0]);

                        if (target == null) {
                            sender.sendMessage(Messages.TAG + "§cPlayer not found.");
                        } else {
                            Ranks.updateRank(target);
                            sender.sendMessage(Messages.TAG + target.getDisplayName() + "'s §arank has been updated.");
                        }
                    }

                }
            }else{
                player.sendMessage(Messages.noPerms);
            }
        } else {
            Player target = Bukkit.getPlayer(args[0]);
            if (target == null) {
            } else {
                Ranks.updateRank(target);
            }
        }
        return false;
    }
}
