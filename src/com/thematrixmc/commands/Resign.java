package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Resign implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (Ranks.isSrStaff(player)) {
                if (args.length == 0) {
                    sender.sendMessage(Messages.TAG + "§cInvalid syntax. Please use: /resign [user]");
                } else {
                    String target = args[0];
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "setrank " + target + " diamond");
                    sender.sendMessage(Messages.TAG + "§aSet " + target + " to rank diamond.");
                }
            }
        } else {
            if (args.length == 0) {
                sender.sendMessage(Messages.TAG + "§cInvalid syntax. Please use: /resign [user]");
            } else {
                String target = args[0];
                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "setrank " + target + " diamond");
                sender.sendMessage(Messages.TAG + "§aSet " + target + " to rank diamond.");
            }
        }
        return false;
    }
}
