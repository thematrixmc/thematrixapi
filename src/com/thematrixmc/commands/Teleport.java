package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Teleport implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (Ranks.isSrStaff(player) || Ranks.isSrBuilder(player)) {
                if (args.length == 0) {
                    player.sendMessage("§cUsage: /tp [player] <target>");
                } else if (args.length == 1) {
                    if (Bukkit.getPlayerExact(args[0]) == null) {
                        player.sendMessage("Can't find player " + args[0] + ". No tp.");
                    } else {
                        Player target = Bukkit.getPlayerExact(args[0]);
                        player.teleport(target);
                        player.sendMessage("Teleported " + player.getDisplayName() + " to " + target.getDisplayName());
                        for (Player srstaff : Bukkit.getOnlinePlayers()) {
                            if (srstaff.hasPermission("matrix.srstaff") && srstaff != player) {
                                srstaff.sendMessage("§7§o[" + player.getName() + ": Teleported " + player.getDisplayName() + " to " + target.getDisplayName() + "§7§o]");
                            }
                            if (srstaff.hasPermission("matrix.rank.srbuilder") && srstaff != player && srstaff != player && !srstaff.hasPermission("matrix.srstaff")) {
                                srstaff.sendMessage("§7§o[" + player.getName() + ": Teleported " + player.getDisplayName() + " to " + target.getDisplayName() + "§7§o]");
                            }
                        }
                    }
                } else if (args.length == 2) {
                    if (Bukkit.getPlayerExact(args[0]) == null) {
                        player.sendMessage("Player not found: " + args[0]);
                    } else if (Bukkit.getPlayerExact(args[1]) == null) {
                        player.sendMessage("Can't find player " + args[1] + ". No tp.");
                    } else {
                        Player target1 = Bukkit.getPlayerExact(args[0]);
                        Player target2 = Bukkit.getPlayerExact(args[1]);
                        target1.teleport(target2);
                        player.sendMessage("Teleported " + target1.getDisplayName() + " to " + target2.getDisplayName());
                        for (Player srstaff : Bukkit.getOnlinePlayers()) {
                            if (srstaff.hasPermission("matrix.srstaff") && srstaff != player) {
                                srstaff.sendMessage("§7§o[" + player.getName() + ": Teleported " + target1.getDisplayName() + " to " + target2.getDisplayName() + "§7§o]");
                            }
                            if (srstaff.hasPermission("matrix.rank.srbuilder") && srstaff != player && !srstaff.hasPermission("matrix.srstaff")) {
                                srstaff.sendMessage("§7§o[" + player.getName() + ": Teleported " + target1.getDisplayName() + " to " + target2.getDisplayName() + "§7§o]");
                            }
                        }
                    }
                } else {
                    player.sendMessage("§cUsage: /tp [player] <target>");
                }

            } else {
                player.sendMessage(Messages.noPerms);
            }
        } else {
            sender.sendMessage(Messages.noPerms);
        }
        return false;
    }

}
