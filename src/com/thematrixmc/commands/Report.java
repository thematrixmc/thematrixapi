package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import com.thematrixmc.thematrixapi.TheMatrixAPI;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Report implements CommandExecutor {

    public ArrayList<String> cooldowns = new ArrayList<>();
    public static Connection connection = TheMatrixAPI.conn;

    public int cooldownTime = 10;
    public long secondsForCooldown = cooldownTime * 20;

    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date date = new Date();
    Calendar cal = Calendar.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String args[]) {
        if (sender instanceof Player) {
            final Player player = (Player) sender;
            if (cooldowns.contains(sender.getName())) {
                sender.sendMessage(Messages.TAG + "§cYou can only report players every 10 seconds!");
            } else {
                if (args.length <= 1) {
                    sender.sendMessage(Messages.TAG + "§cInvalid syntax. Please use: /report [name] [reason]");
                } else if (Bukkit.getPlayer(args[0]) != null) {
                    Player target = Bukkit.getPlayer(args[0]);
                    String cast = "";
                    for (String str : args) {
                        cast = (cast + str + " ");
                    }
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        if (Ranks.isStaff(pl)) {
                            pl.sendMessage(Messages.STAFF + "§eREPORT " + "§8\u2759 " + player.getDisplayName() + " §8» §f" + cast);
                        }
                    }
                    try {
                        File log = new File("/API files/reports.txt");
                        FileWriter fileWriter = new FileWriter(log, true);
                        try (PrintWriter out = new PrintWriter(fileWriter)) {
                            log.createNewFile();
                            out.println(dateFormat.format(date) + " | " + player.getName() + " Reported " + target.getName() + " for " + cast);
                            out.close();

                            log(dateFormat.format(date), target, player, true, player.getName() + " reported " + target.getName() + " for " + cast);
                        } catch (SQLException e){

                        }
                    } catch (IOException e) {
                        
                    }
                    sender.sendMessage(Messages.TAG + "§aMessage sent. §6Please don't spam this command.");
                    sender.sendMessage(Messages.TAG + "§cThis is logged.");
                    cooldowns.add(sender.getName());
                    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(TheMatrixAPI.getPlugin(TheMatrixAPI.main), new Runnable() {
                        @Override
                        public void run() {
                            cooldowns.remove(player.getName());
                        }
                    }, secondsForCooldown);
                } else {
                    player.sendMessage(Messages.TAG + "§c" + args[0] + " is not an online player.");
                }
            }
        }

        return false;
    }

    public static void log(String date, Player reported, Player reporting, boolean successful, String details) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("INSERT INTO `reportLog` (`Date`,`Reported`,`Reporting`,`Successful`,`Details`) VALUES ('" + date + "','" + reported.getName() + "', '" + reporting.getName() + "', '" + successful + "', '" + details + "');");
    }
}
