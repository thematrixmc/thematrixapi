package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Msg implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        Player player = (Player) sender;
        if (sender instanceof Player) {
            if (Ranks.isStaff(player)) {
                if (args.length >= 2) {
                    if (!(Bukkit.getPlayer(args[0]) == null)) {
                        Player target = Bukkit.getPlayer(args[0]);
                            String msg = "";
                            for (int i = 1; i < args.length; i++) {
                                String arg = args[i] + " ";
                                msg = msg + arg;
                            }
                            
                            for (Player pl : Bukkit.getOnlinePlayers()) {
                                if(pl == target){
                                    pl.sendMessage("\u00A74PRIVATE \u00A78\u2759 " + player.getDisplayName() + " \u00A78» \u00A77" + msg);
                                } else if (Ranks.isStaff(pl)) {
                                    pl.sendMessage("\u00A74PRIVATE \u00A78\u2759 " + player.getDisplayName() + " \u00A77\u27A0 " + target.getDisplayName() + " \u00A78» \u00A77" + msg);
                                }
                            }
                    } else {
                        sender.sendMessage(Messages.TAG + "§cPlayer not online.");
                    }
                } else if (args.length <= 1) {
                    sender.sendMessage(Messages.TAG + "§cInvalid syntax. Usage: /msg [Name] [Message]");
                }
            } else {
                sender.sendMessage(Messages.TAG + "§cYou do not have.. wait what. #NoPerms");
            }
        }
        return false;
    }
}
