package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class Clear implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        Player player = (Player) sender;
        if (sender instanceof Player) {
            if (Ranks.isSrStaff(player)) {
                for (Entity e : player.getWorld().getEntities()) {
                    if (!(e instanceof Player)) {
                        e.remove();
                    }
                }
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    if (Ranks.isStaff(pl)) {
                        pl.sendMessage(Messages.TAG + "§6Entities cleared by " + player.getDisplayName() + "§6.");
                    }
                }
            }
        }
        return false;
    }
}
