package com.thematrixmc.commands;

import static com.thematrixmc.matrixantispam.SwearFilter.toggled;
import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ToggleSwears implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (Ranks.isSrStaff(player)) {
                if (args.length > 0) {
                    player.sendMessage(Messages.TAG + "§cIncorrect syntax. Please use: /toggleswears");
                } else {
                    if (!toggled.contains(player.getName())) {
                        toggled.add(player.getName());
                        player.sendMessage(Messages.TAG + "§aSwears toggled: §aon");
                    } else {
                        toggled.remove(player.getName());
                        player.sendMessage(Messages.TAG + "§aSwears toggled: §coff");
                    }
                }
            } else {
                player.sendMessage(Messages.noPerms);
            }          
        } else {
            sender.sendMessage(Messages.noPerms);
        }
        
        return true;
    }
}
