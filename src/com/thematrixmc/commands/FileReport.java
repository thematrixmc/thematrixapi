package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.TheMatrixAPI;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class FileReport implements CommandExecutor {

    public static Date date = new Date();
    public static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    public static ArrayList<String> reporting = new ArrayList<>();
    public static ArrayList<String> step1 = new ArrayList<>();
    public static ArrayList<String> step2 = new ArrayList<>();
    public static ArrayList<String> step3 = new ArrayList<>();
    public static ArrayList<String> step4 = new ArrayList<>();

    public static ArrayList<String> step1check = new ArrayList<>();
    public static ArrayList<String> step2check = new ArrayList<>();
    public static ArrayList<String> step3check = new ArrayList<>();
    public static ArrayList<String> step4check = new ArrayList<>();

    public HashMap<String, String> step1Answer = new HashMap<>();
    public HashMap<String, String> step2Answer = new HashMap<>();
    public HashMap<String, String> step3Answer = new HashMap<>();
    public HashMap<String, String> step4Answer = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length > 0) {
                player.sendMessage(Messages.TAG + "§cIncorrect syntax. Please use: /filereport.");
            } else {
                if (!reporting.contains(player.getName())) {
                    enableReporting(player);
                    player.sendMessage(Messages.reportingTag + "§8--------------------========================--------------------");
                    player.sendMessage(Messages.reportingTag + "§6Welcome to the Report wizard.");
                    player.sendMessage(Messages.reportingTag + "§6This wizard is in §3EARLY BETA§6, so please report any bugs found to bugswatter@thematrixmc.com.");
                    player.sendMessage(Messages.reportingTag + "§8--------------------========================--------------------");
                    player.sendMessage(Messages.reportingTag + "");
                    player.sendMessage(Messages.reportingTag + "§61) §eWho is it that you wish to report?");
                    player.sendMessage(Messages.reportingTag + "§3We need their full Minecraft username please.");
                } else {
                    if (step2.contains(player.getName())) {
                        player.sendMessage(Messages.reportingTag + "§aThank you.");
                        player.sendMessage(Messages.reportingTag + "§62) §eWhy are you reporting this person?");
                        player.sendMessage(Messages.reportingTag + "§3As full as possible, please.");
                    } else if (step3.contains(player.getName())) {
                        player.sendMessage(Messages.reportingTag + "§aThank you.");
                        player.sendMessage(Messages.reportingTag + "§63) §eAny other additional details?");
                        player.sendMessage(Messages.reportingTag + "§3Again, as full as possible, please.");
                    } else if (step4.contains(player.getName())) {
                        player.sendMessage(Messages.reportingTag + "§aThank you.");
                        player.sendMessage(Messages.reportingTag + "§63) §ePlease link a screenshot.");
                        player.sendMessage(Messages.reportingTag + "§3Sufficient evidence and/or proof of point is needed for our moderator's bans.");
                    } else {
                        player.sendMessage(Messages.reportingTag + "§8--------------------========================--------------------");
                        player.sendMessage(Messages.reportingTag + "§6Thanks for using the Report wizard.");
                        player.sendMessage(Messages.reportingTag + "§3Your information has been passed to our moderators and is being dealt with appropriately. Thank you.");
                        player.sendMessage(Messages.reportingTag + "§8--------------------========================--------------------");
                        player.sendMessage(Messages.reportingTag + "");

                        log(dateFormat.format(date), player, Bukkit.getPlayer(step1Answer.get(player.getName())), step2Answer.get(player.getName()), getServerName(), step3Answer.get(player.getName()), step4Answer.get(player.getName()));
                        disableReporting(player);
                    }
                }
            }
        } else {
            sender.sendMessage("FKOF CONSOLE");
        }
        return true;
    }

    public static void enableReporting(Player player) {
        reporting.add(player.getName());
        step1.add(player.getName());
    }

    public static void disableReporting(Player player) {
        reporting.remove(player.getName());
        step1.remove(player.getName());
        step2.remove(player.getName());
        step3.remove(player.getName());
        step4.remove(player.getName());
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = (Player) event.getPlayer();
        if (reporting.contains(player.getName()) && !event.getMessage().startsWith("/") && !event.getMessage().equalsIgnoreCase("Yes")) {
            event.setCancelled(true);
            player.sendMessage(Messages.reportingTag + "§6You entered: §3" + event.getMessage() + "§6. Is this information correct?");
            player.sendMessage(Messages.reportingTag + "§3Confirm by saying 'Yes' in chat.");
        } else if (reporting.contains(player.getName()) && event.getMessage().equalsIgnoreCase("Yes")) {
            if (step1check.contains(player.getName())) {
                event.setCancelled(true);
                step1Answer.put(player.getName(), event.getMessage());
                step1.remove(player.getName());
                step2.add(player.getName());
                player.chat("/filereport");
            } else if (step2check.contains(player.getName())) {
                event.setCancelled(true);
                step2Answer.put(player.getName(), event.getMessage());
                step3.remove(player.getName());
                step3.add(player.getName());
                player.chat("/filereport");
            } else if (step3check.contains(player.getName())) {
                event.setCancelled(true);
                step3Answer.put(player.getName(), event.getMessage());
                step3.remove(player.getName());
                step4.add(player.getName());
                player.chat("/filereport");
            } else if (step4check.contains(player.getName())) {
                event.setCancelled(true);
                step4Answer.put(player.getName(), event.getMessage());
                step4.remove(player.getName());
                player.chat("/filereport");
            }
        }
    }

    public static void log(String date, Player reported, Player reporting, String reason, String server, String extra, String evidence) {
        try {
            Statement statement = TheMatrixAPI.conn.createStatement();
            statement.executeUpdate("INSERT INTO `report` VALUES ('" + date + "','" + reported.getName() + "','" + reporting.getName() + "','" + reason + "','" + server + "','" + extra + "','" + evidence + "');");
        } catch (SQLException ex) {

        }
    }

    private String getServerName() {
        if(TheMatrixAPI.FFA){
            return "FFA";
        } else if(TheMatrixAPI.Hub){
            return "Hub";
        } else if(TheMatrixAPI.TDM){
            return "TDM";
        } else {
            return "Unknown";
        }
    }

}
