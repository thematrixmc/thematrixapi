package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Live implements CommandExecutor {

    public HashMap<String, Long> cooldowns = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        Player player = (Player) sender;
        if ((Ranks.isFriend(player)) || (Ranks.isYT(player)) || (Ranks.isStaff(player))) {
            if (args.length == 1) {
                int cooldownTime = 30;
                if (cooldowns.containsKey(sender.getName().toLowerCase())) {
                    long secondsLeft = ((cooldowns.get(sender.getName().toLowerCase()) / 120000) + cooldownTime) - (System.currentTimeMillis() / 120000);
                    if (secondsLeft > 0) {
                        sender.sendMessage(Messages.TAG + "§cYou can't do this command for another §6" + secondsLeft + " minutes§c!");
                    }
                } else {
                    if (args[0].toLowerCase().equals("me")) {
                        cooldowns.put(player.getName().toLowerCase(), System.currentTimeMillis());
                        Bukkit.broadcastMessage("§4\u25CF §eLIVE NOW \u2759 " + player.getDisplayName() + " §8\u279C §6http://twitch.tv/" + player.getName().toLowerCase());
                    } else if (cooldowns.containsKey(args[0].toLowerCase())) {
                        long secondsLeft = ((cooldowns.get(sender.getName().toLowerCase()) / 120000) + cooldownTime) - (System.currentTimeMillis() / 120000);
                        sender.sendMessage(Messages.TAG + "§cThis stream has already been advertised. \nPlease wait §6" + secondsLeft + " minutes§c before advertising again.");
                    } else {
                        cooldowns.put(sender.getName().toLowerCase(), System.currentTimeMillis());
                        cooldowns.put(args[0].toLowerCase(), System.currentTimeMillis());
                        Bukkit.broadcastMessage("§4\u25CF §eLIVE NOW \u2759 " + player.getDisplayName() + " §8\u279C §6http://twitch.tv/" + args[0].toLowerCase());
                    }
                }
            } else {
                sender.sendMessage(Messages.TAG + "§cInvalid syntax. Please use:\n '/live me' for your in game name or '/live [StreamName]'");
            }
        }
        return false;
    }
}