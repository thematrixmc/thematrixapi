package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import java.util.ArrayList;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ToggleRank implements CommandExecutor {

    public static ArrayList<Player> toggled = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (!toggled.contains(player)) {
                if (Ranks.getRankID(player) > 1) {
                    toggled.add(player);
                    Ranks.updateRank(player);
                    player.sendMessage(Messages.TAG + "§aRank toggled: §coff");
                } else {
                    player.sendMessage("Unknown command. Type \"/help\" for help.");
                }
            } else {
                toggled.remove(player);
                Ranks.updateRank(player);
                player.sendMessage(Messages.TAG + "§aRank toggled: §aon");
            }
        }
        return false;
    }
}
