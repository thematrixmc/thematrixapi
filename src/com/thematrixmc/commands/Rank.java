package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Rank implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            sender.sendMessage(Messages.TAG + "§6All of the MatrixMC ranks: §aOwner§6, §eDeveloper§6, §2StaffManager§6, §5Admin§6, §4SeniorMod§6, §cModerator§6, §1Builder§6, §dYoutuber§6, §3Friend§6, §bDiamond§6, §6Gold§6, §7Iron§6, and §9Default§6.");
        }
        return false;
    }
}