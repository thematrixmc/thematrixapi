package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class GetRank implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (args.length != 0) {
            if (PermissionsEx.getUser(args[0]) != null) {
                if (args.length > 0) {
                    sender.sendMessage(Messages.TAG + "§6" + args[0] + " is rank " + getRank(args[0]));
                    return false;
                }
            } else {
                sender.sendMessage(Messages.TAG + "§6" + args[0] + " has never played before.");
            }
        } else {
            sender.sendMessage(Messages.TAG + "§cInvalid syntax. Please use: /getrank [name]");
        }
        return false;
    }

    public String getRank(String player) {
        if (player.equalsIgnoreCase("TysonPhilip") || player.equalsIgnoreCase("Stargate_")){
            return "§3Friend";
        } else if (player.equalsIgnoreCase("Peter_Forties")){
            return "§3Friend";
        }else if (PermissionsEx.getUser(player).has("matrix.rank.owner")) {
            return "§aOwner";
        } else if (PermissionsEx.getUser(player).has("matrix.rank.developer")) {
            return "§eDeveloper";
        } else if (PermissionsEx.getUser(player).has("matrix.rank.staffmanager")) {
            return "§2StaffManager";
        } else if (PermissionsEx.getUser(player).has("matrix.rank.admin")) {
            return "§5Admin";
        } else if (PermissionsEx.getUser(player).has("matrix.rank.srmod")) {
            return "§4SeniorMod";
        } else if (PermissionsEx.getUser(player).has("matrix.rank.mod")) {
            return "§cModerator";
        } else if (PermissionsEx.getUser(player).has("matrix.rank.srbuilder")) {
            return "§1Senior Builder";
        } else if (PermissionsEx.getUser(player).has("matrix.rank.builder")) {
            return "§1Builder";
        } else if (PermissionsEx.getUser(player).has("matrix.rank.yt")) {
            return "§dYoutuber";
        } else if (PermissionsEx.getUser(player).has("matrix.rank.friend")) {
            return "§3Friend";
        } else if (PermissionsEx.getUser(player).has("matrix.rank.diamond")) {
            return "§bDiamond";
        } else if (PermissionsEx.getUser(player).has("matrix.rank.gold")) {
            return "§6Gold";
        } else if (PermissionsEx.getUser(player).has("matrix.rank.iron")) {
            return "§7Iron";
        } else {
            return "§9Default";
        }
    }
}
