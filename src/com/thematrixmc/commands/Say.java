package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.Ranks;
import java.util.ArrayList;
import java.util.Arrays;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import java.util.List;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

public class Say implements CommandExecutor, TabExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String args[]) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (Ranks.isStaff(player) || Ranks.isSrBuilder(player)) {
                if (args.length == 0) {
                    sender.sendMessage(Messages.TAG + ChatColor.DARK_AQUA + "Usage: /say [message]");
                } else {
                    if (args.length < 1) {
                        return true;
                    }

                    String cast = "";
                    for (String str : args) {
                        cast = (cast + str + " ");
                    }
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        if (Ranks.isOwner(player) && !(player.getName().equalsIgnoreCase("TysonPhilip")) && !(player.getName().equalsIgnoreCase("Stargate_")) && !(player.getName().equalsIgnoreCase("Peter_Forties"))) {
                            pl.sendMessage(ChatColor.GREEN + "OWNER \u275a " + player.getName() + ChatColor.WHITE + ": " + ChatColor.DARK_GREEN + cast);
                        } else if (Ranks.isDeveloper(player)) {
                            pl.sendMessage(ChatColor.YELLOW + "DEVELOPER \u275a " + player.getName() + ChatColor.WHITE + ": " + ChatColor.DARK_GREEN + cast);
                        } else if (Ranks.isAdmin(player)) {
                            pl.sendMessage(ChatColor.DARK_PURPLE + "ADMIN \u275a " + player.getName() + ChatColor.WHITE + ": " + ChatColor.DARK_GREEN + cast);
                        } else if (Ranks.isStaffManager(player)) {
                            pl.sendMessage(ChatColor.DARK_GREEN + "STAFF MANAGER \u275a " + player.getName() + ChatColor.WHITE + ": " + ChatColor.LIGHT_PURPLE + cast);
                        } else if (Ranks.isSrMod(player)) {
                            pl.sendMessage(ChatColor.DARK_RED + "SR.MOD \u275a " + player.getName() + ChatColor.WHITE + ": " + ChatColor.DARK_GREEN + cast);
                        } else if (Ranks.isMod(player)) {
                            pl.sendMessage(ChatColor.RED + "MODERATOR \u275a " + player.getName() + ChatColor.WHITE + ": " + ChatColor.DARK_GREEN + cast);
                        } else if (Ranks.isSrBuilder(player)) {
                            pl.sendMessage(ChatColor.DARK_BLUE + "SR.BUILDER \u275a " + player.getName() + ChatColor.WHITE + ": " + ChatColor.DARK_GREEN + cast);
                        } else {
                            pl.sendMessage(ChatColor.DARK_AQUA + "FRIEND \u275a " + player.getName() + ChatColor.WHITE + ": " + ChatColor.DARK_GREEN + cast);
                        }
                    }
                }
            } else {
                sender.sendMessage(Messages.noPerms);
            }
        } else {
            sender.sendMessage(Messages.noPerms);
        }

        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("team")) {
            List<String> matches = new ArrayList<>();
            if (args.length == 1) {
                String search = args[0].toLowerCase();
                List<String> arg1 = Arrays.asList("create", "add", "colour");
                for (String string : arg1) {
                    if (string.toLowerCase().startsWith(search)) {
                        matches.add(string);
                    }
                }
            }
            return matches;
        } else {
            return null;
        }
    }

}
