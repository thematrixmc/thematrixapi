package com.thematrixmc.commands;

import com.thematrixmc.thematrixapi.Ranks;
import static com.thematrixmc.thematrixapi.TheMatrixAPI.FFA;
import static com.thematrixmc.thematrixapi.TheMatrixAPI.Hub;
import static com.thematrixmc.thematrixapi.TheMatrixAPI.TDM;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Help implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String args[]) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 0) {
                sender.sendMessage("§7▅ ▆ ▇ §eMatrixMC Commands §7▇ ▆ ▅");
                //sender.sendMessage("§7\u27BB §9/blameliam §7- §6#BlameLiam");
                sender.sendMessage("§7\u27BB §9/stafflist §7- §6Lists all staff on the current server.");
                sender.sendMessage("§7\u27BB §9/ranks §7- §6Shows all ranks.");
                sender.sendMessage("§7\u27BB §9/help §7- §6Shows this menu.");
                sender.sendMessage("§7\u27BB §9/report §7- §6Sends a message to all online staff.");
                sender.sendMessage("§7\u27BB §9/ping §7- §6Gets users ping.");
                sender.sendMessage("§7\u27BB §9/list §7- §6Shows amount of online players.");
                if (FFA == true) {
                    sender.sendMessage("§7\u27BB §9/fix §7- §6Ghost fix yourself");
                }
                if (TDM == true) {
                    sender.sendMessage("§7\u27BB §9/score §7- §6Current match score.");
                    sender.sendMessage("§7\u27BB §9/tl §7- §6Time left until match end.");
                    sender.sendMessage("§7\u27BB §9/t [message] §7- §6Talk to your team only.");
                }
                if (Ranks.isStaff(player)) {
                    sender.sendMessage("§7\u27BB §c/help staff §7- §6Shows Staff commands.");
                }
                if (Ranks.isSrStaff(player)) {
                    sender.sendMessage("§7\u27BB §4/help srstaff §7- §6Shows SrStaff commands.");
                }
                if ((Ranks.isYT(player)) || (Ranks.isFriend(player)) || (Ranks.isStaff(player))) {
                    sender.sendMessage("§7\u27BB §d/live §7- §6Broadcast http://twitch.tv/[YourNameHere]");
                }
                sender.sendMessage("§7▔▔▔▀▀▀██ §cEND §7██▀▀▀▔▔▔");
            }
            if (args.length == 1) {
                if ((Ranks.isStaff(player)) && ("staff".equals(args[0]))) {
                    sender.sendMessage("§7▅ ▆ ▇ §eMatrixMC §cStaff §eCommands §7▇ ▆ ▅");
                    sender.sendMessage("§7\u27BB §c/staff, /s §7- §6Broadcast staff only message.");
                    sender.sendMessage("§7\u27BB §c/st §7- §6Opens staff tool GUI.");
                    sender.sendMessage("§7\u27BB §c/tempban §7- §6Temp bans player.");
                    sender.sendMessage("§7\u27BB §c/kick §7- §6Kicks player from network.");
                    sender.sendMessage("§7\u27BB §c/whereis §7- §6Gets a players server.");
                    sender.sendMessage("§7\u27BB §c/say §7- §6Broadcasts a message.");
                    sender.sendMessage("§7\u27BB §c/spectate §7- §6Teleports you to a player.");
                    sender.sendMessage("§7\u27BB §c/vanish §7- §6Set you invisible and able to fly.");
                    sender.sendMessage("§7\u27BB §c/clearchat §7- §6Clears the chat.");
                    sender.sendMessage("§7▔▔▔▀▀▀▀██ §cEND §7██▀▀▀▀▔▔▔");
                }
                if ((Ranks.isSrStaff(player)) && ("srstaff".equals(args[0]))) {
                    sender.sendMessage("§7▅ ▆ ▇ §eMatrixMC §4Senior Staff §eCommands §7▇ ▆ ▅");
                    sender.sendMessage("§7\u27BB §4/unban §7- §6Unbans user.");
                    sender.sendMessage("§7\u27BB §4/ban §7- §6Perma ban user.");
                    sender.sendMessage("§7\u27BB §4/clear §7- §6Clears all entities.");
                    sender.sendMessage("§7\u27BB §4/tp §7- Teleport to a player.");
                    sender.sendMessage("§7\u27BB §4/resign §7- §6Sets target to diamond.");
                    sender.sendMessage("§7\u27BB §4/gm, /gma, /gmc, /gms §7- §6Changes your gamemode.");
                    sender.sendMessage("§7\u27BB §4/maintenance §7- §6Toggles maintenance mode.");
                    sender.sendMessage("§7\u27BB §9/gblamealex §7- §6#BlameAlex");
                    if (Hub == true && Ranks.isDeveloper(player)) {
                        sender.sendMessage("§7\u27BB §4/fwparty §7- §6Throws fireworks every direction.");
                    }
                    sender.sendMessage("§7▔▔▔▀▀▀▀▀███ §cEND §7███▀▀▀▀▀▔▔▔");
                }
            }
        }
        return false;
    }
}
