package com.thematrixmc.commands;

import static com.thematrixmc.stafftools.VGUIHandler.vanished;
import com.thematrixmc.thematrixapi.Messages;
import com.thematrixmc.thematrixapi.NameColours;
import com.thematrixmc.thematrixapi.Ranks;
import com.thematrixmc.thematrixapi.TheMatrixAPI;
import static com.thematrixmc.thematrixapi.TheMatrixAPI.TDM;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetRank implements CommandExecutor {

    public static Connection connection = TheMatrixAPI.conn;

    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date date = new Date();
    Calendar cal = Calendar.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (Ranks.isSrStaff(player)) {
                if (args.length <= 1) {
                    sender.sendMessage(Messages.TAG + "§cInvalid syntax. Please use: /setrank [user] [rank]");
                } else {
                    if (Bukkit.getOfflinePlayer(args[0]) != null) {
                        try {
                            File log = new File("/API files/setranklog.txt");
                            FileWriter fileWriter = new FileWriter(log, true);
                            try (PrintWriter out = new PrintWriter(fileWriter)) {
                                log.createNewFile();
                                out.println(dateFormat.format(date) + " | " + player.getName() + " set " + args[0] + " to " + args[1]);
                                out.close();
                            }

                            log(dateFormat.format(date), Bukkit.getPlayer(args[0]), player, true, player.getName() + " set " + args[0] + " to " + args[1]);
                        } catch (IOException | SQLException e) {
                        }
                        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "pex user " + Bukkit.getOfflinePlayer(args[0]).getUniqueId() + " group set " + args[1]);
                        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "pex user " + args[0] + " group set " + args[1]);
                        if (Bukkit.getPlayer(args[0]) != null) {
                            Player target = Bukkit.getPlayer(args[0]);
                            if (Ranks.isSrStaff(target)) {
                                for (Player pl : vanished) {
                                    target.showPlayer(pl);
                                }
                            }
                            if (TDM == false) {
                                NameColours.Join(target);
                            }
                            Ranks.updateRank(target);
                            sender.sendMessage(Messages.TAG + target.getDisplayName() + "'s §arank updated.");
                        } else {
                            sender.sendMessage(Messages.TAG + "§cOffline player: §6" + args[0] + "'s §arank set to " + args[1] + "§a.");
                        }
                    } else {
                        sender.sendMessage(Messages.TAG + "§c" + args[0] + " has never played before.");
                    }
                }
            }
        } else {
            if (args.length <= 1) {
                sender.sendMessage(Messages.TAG + "§cInvalid syntax. Please use: /setrank [user] [rank]");
            } else {
                if (Bukkit.getOfflinePlayer(args[0]).getUniqueId() != null) {
                    UUID target = Bukkit.getOfflinePlayer(args[0]).getUniqueId();
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "pex user " + target + " group set " + args[1]);
                    if (Bukkit.getPlayer(args[0]) != null) {
                        if (Ranks.isSrStaff(Bukkit.getPlayer(args[0]))) {
                            for (Player pl : vanished) {
                                Bukkit.getPlayer(args[0]).showPlayer(pl);
                            }
                        }
                        if (TDM == false) {
                            NameColours.Join(Bukkit.getPlayer(args[0]));
                        }
                        Ranks.updateRank(Bukkit.getPlayer(args[0]));
                        sender.sendMessage(Messages.TAG + "§a" + Bukkit.getPlayer(args[0]).getDisplayName() + "'s §arank updated.");
                    } else {
                        sender.sendMessage(Messages.TAG + "§cOffline player: §6" + args[0] + "'s §arank set to " + args[1] + "§a.");
                    }
                } else {
                    sender.sendMessage(Messages.TAG + "§c" + args[0] + " has never played before.");
                }
            }
        }
        return false;
    }

    public static void log(String date, Player rankSet, Player whoSetRank, boolean successful, String details) throws SQLException {
        Statement statement = connection.createStatement();
        if (statement.getMoreResults() == false) {
            throw new SQLException("Can't get more results.");
        }
        statement.executeUpdate("INSERT INTO `setrankLog` (`Date`,`PlayerRankSet`,`WhoSetRank`,`Successful`,`Details`) VALUES ('" + date + "','" + rankSet.getName() + "', '" + whoSetRank.getName() + "', '" + successful + "', '" + details + "');");
    }
}
