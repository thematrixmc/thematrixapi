package com.thematrixmc.commands;

import com.thematrixmc.currencies.BulletAPI;
import com.thematrixmc.thematrixapi.Messages;
import java.sql.SQLException;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Bullets implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender instanceof Player){
            Player player = (Player) sender;
                try {
                    player.sendMessage(Messages.TAG + "§eYour current bullet balance is: §d" + BulletAPI.getBullets(player) + "§e.");
                } catch (SQLException ex) {
                    
                }
        } else {
          return true;  
        }
        return true;
    }
}